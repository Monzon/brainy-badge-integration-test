package com.avantica.brainy.controllers;

import com.avantica.brainy.Constants;
import com.avantica.brainy.helpers.DataSourceClient;
import io.restassured.http.ContentType;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class BadgeControllerCreationIntegrationTest extends AbstractControllerIntegrationTest {

    @Autowired
    private Constants constants;

    @Autowired
    private DataSourceClient dataSourceClient;

    @Before
    public void setUp() {
        dataSourceClient.cleanBadgeCollection();
    }

    @Test
    public void shouldReturnCorrelativeAckWhenNotifyingABadgeAction() {
        String json = jsonForBadgeAction();

        ExtractableResponse<Response> ackResponse1 = notifyBadgeAction(json)
                .extract();

        ExtractableResponse<Response> ackResponse2 = notifyBadgeAction(json)
                .extract();

        Integer idAck1 = ackResponse1.body().jsonPath().get("id");
        Integer idAck2 = ackResponse2.body().jsonPath().get("id");

        assertThat(idAck2, is(idAck1 + 1));
    }

    @Test
    public void shouldReturnOkWhenConfirmBadgeAction() {
        String json = jsonForBadgeAction();

        ExtractableResponse<Response> ackResponse1 = notifyBadgeAction(json)
                .extract();

        Integer idAck1 = ackResponse1.body().jsonPath().get("id");

        given().
                contentType(ContentType.JSON).
                body("{\"id\":" + idAck1 + "}").
        when().
                post(constants.getBrainyBadgeUrl() + "profile/terminator@avantica.net/badges ").
        then().
                assertThat().
                statusCode(is(200)).
                and().
                body("email", is("terminator@avantica.net"));
    }

    private ValidatableResponse notifyBadgeAction(String json) {
        return
                given().
                        contentType(ContentType.JSON).
                        body(json).
                when().
                        post(constants.getBrainyBadgeUrl() + "profile/terminator@avantica.net/badges/acks").
                then().
                        assertThat().
                        statusCode(is(201)).
                        and().
                        body("id", is(greaterThan(0)));
    }

    private String jsonForBadgeAction() {
        return "{\n" +
                "   \"action\":\"save-profile\",\n" +
                "   \"arguments\":{\n" +
                "      \"id\":1,\n" +
                "      \"names\":\"terminator\",\n" +
                "      \"email\":\"terminator@avantica.net\",\n" +
                "      \"summary\":\"summary33\",\n" +
                "      \"position\":\"position341\",\n" +
                "      \"skype\":\"skype382\",\n" +
                "      \"location\":\"location8\",\n" +
                "      \"englishLevel\":\"basic38\",\n" +
                "      \"skillList\":[\n" +
                "         {\n" +
                "            \"name\":\"java\"\n" +
                "         },\n" +
                "         {\n" +
                "            \"name\":\"angularjs\"\n" +
                "         },\n" +
                "         {\n" +
                "            \"name\":\"java\"\n" +
                "         },\n" +
                "         {\n" +
                "            \"name\":\"angularjs\"\n" +
                "         },\n" +
                "         {\n" +
                "            \"name\":\"java\"\n" +
                "         },\n" +
                "         {\n" +
                "            \"name\":\"angularjs\"\n" +
                "         }\n" +
                "      ]\n" +
                "   }\n" +
                "}";
    }

}
