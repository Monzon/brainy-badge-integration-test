package com.avantica.brainy.controllers;

import com.avantica.brainy.Constants;
import com.avantica.brainy.helpers.DataSourceClient;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;

public class BadgeControllerQueryIntegrationTest extends AbstractControllerIntegrationTest {

    @Autowired
    private Constants constants;

    @Autowired
    private DataSourceClient dataSourceClient;

    @Before
    public void setUp() {
        dataSourceClient
                .cleanBadgeCollection()
                .fillBadgesTest();
    }

    @Test
    public void shouldReturnOkStatusWhenRetrievingBadgesByEmail() {
        given().
                contentType(ContentType.JSON).
        when().
                get(constants.getBrainyBadgeUrl() + "profile/terminator@avantica.net/badges").
        then().
                assertThat().
                statusCode(is(HttpStatus.SC_OK)).
                and().
                body("elements.name", hasItems("Skill ninja master", "Skill master", "Skill champion"));
    }

}
