package com.avantica.brainy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Constants {
    @Value("${brainy.badge.host:badge}")
    private String brainyBadgeHost;

    @Value("${brainy.badge.port:9000}")
    private String brainyBadgePort;

    public String getBrainyBadgeUrl() {
        return String.format("http://%s:%s/", brainyBadgeHost, brainyBadgePort);
    }
}
