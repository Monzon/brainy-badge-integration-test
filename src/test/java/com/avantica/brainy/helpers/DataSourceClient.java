package com.avantica.brainy.helpers;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class DataSourceClient {

    @Value("${brainy.mongodb.host:bitnami__mongodb}")
    private String mongoHost;

    @Value("${brainy.mongodb.port:27017}")
    private String mongoPort;

    @Value("${brainy.mongodb.user:test}")
    private String mongoUser;

    @Value("${brainy.mongodb.password:test}")
    private String mongoPassword;

    @Value("${brainy.mongodb.database:badge_db}")
    private String mongoDatabase;

    private MongoClient mongoClient;
    private MongoDatabase database;


    public DataSourceClient cleanBadgeCollection() {
        loadDatabase();
        MongoCollection<Document> collection = database.getCollection("userBadgeLists");
        collection.drop();
        return this;
    }

    public void fillBadgesTest() {
        loadDatabase();
        MongoCollection<BasicDBObject> collection = database.getCollection("userBadgeLists", BasicDBObject.class);
        String json = "{\n" +
                "    \"profile_id\": \"1\",\n" +
                "    \"email\": \"terminator@avantica.net\",\n" +
                "    \"badges\": [\n" +
                "        {\n" +
                "            \"name\": \"Skill ninja master\",\n" +
                "            \"color\": \"rgba(255, 40, 40, 0.75)\",\n" +
                "            \"imageUrl\": \"\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"Skill master\",\n" +
                "            \"color\": \"rgba(55, 40, 140, 0.75)\", \n" +
                "            \"imageUrl\": \"\"\n" +
                "        },\n" +
                "        {    \n" +
                "            \"name\": \"Skill champion\",\n" +
                "            \"color\": \"rgba(25, 140, 40, 0.75)\",\n" +
                "            \"imageUrl\": \"\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        BasicDBObject dbObject = (BasicDBObject) JSON.parse(json);
        collection.insertOne(dbObject);
    }

    private void loadDatabase() {
        MongoCredential credential = MongoCredential.createCredential(mongoUser, mongoDatabase, mongoPassword.toCharArray());
        ServerAddress serverAddress = new ServerAddress(mongoHost, Integer.parseInt(mongoPort));
        mongoClient = new MongoClient(serverAddress, Arrays.asList(credential));
        database = mongoClient.getDatabase("badge_db");
    }
}
